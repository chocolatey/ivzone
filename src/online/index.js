import Online from 'ivz-online'

export default {
    install(app) {
        app.use(Online)
    }
}
